
const Task = require('./../models/Task');

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then( result => result)
}

//get all
module.exports.getAllTasks = () => {

	return Task.find().then( (result) => result)}




//get specific
	//6197d2ea1c65f51754e55607 //eat
module.exports.getById = (params) => {

	return Task.findById(params).then( result => result)
}



//update status
module.exports.updateTask = (params) => {

	let updatedStatus = {
		status: "complete"
	}

	return Task.findOneAndUpdate({_id: params}, updatedStatus, {new: true}).then(
		result => result)}


//delete
module.exports.deleteTask = (params) => {

	return Task.findOneAndDelete({_id: params}).then( result => {
		return true
	})
}