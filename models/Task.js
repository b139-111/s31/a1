
//in order to use mongoose properties and methods, import mongoose module
const mongoose = require("mongoose");

//Schema
const taskSchema = new mongoose.Schema(
	{
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	}
);

//model
	//we make model out of the schema because model has methods that can manipulate and query database
module.exports = mongoose.model("Task", taskSchema)
