const express = require("express");
const router = express.Router();

const taskController = require("./../controllers/taskControllers")

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

//get all
router.get("/all-task", (req, res) => {

	taskController.getAllTasks().then( result => res.send(result))
})


//get specific
	//6197d2ea1c65f51754e55607 //eat
router.get("/:id", (req, res) => {

	taskController.getById(req.params.id).then( result => res.send(result))
})


//update status
router.put("/:id/complete", (req, res) => {

	taskController.updateTask(req.params.id).then( result => res.send(result))
})


//delete
router.delete("/:id/delete", (req, res) => {

	taskController.deleteTask(req.params.id).then( result => res.send(result))
})


module.exports = router;