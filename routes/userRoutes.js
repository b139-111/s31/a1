
//object that handles client requests and send back responses
//property of express module
	//Router();

const express = require("express");
const router = express.Router();

//in order to use controllers module properties, import controllers module
const userController = require("./../controllers/userControllers");

//Retrieving array of documents from database using "GET" method and find() model method
router.get("/", (req, res) => {
	//check the request if there's data to be used
	// console.log(req);

	//invoke the function from controllers module
	userController.getAllUsers().then( result => {
		res.send(result)
	})
});


//Add a user in the database using "POST" http method and save() method
router.post("/add-user", (req, res) => {
	//check the request if there's data to be used
	console.log(req.body);

	//invoke the function from controllers module
	userController.register(req.body).then( (result) => res.send(result))
})

//update user information
router.put("/update-user", (req, res) => {
	console.log(req.body)	//object

	userController.updateUser(req.body.email).then( result => res.send(result))
})

//delete user
router.delete("/delete-user", (req, res) => {

	userController.deleteUser(req.body.email).then( result => res.send(result))
})

//retrieve specific user using 
	//findOne()
	//findbyId()

router.get("/specific-user", (req, res) => {

	userController.getSpecificUser(req.body.email).then( result => res.send(result))
})

//6197ba889b468515f0c15d74
router.get("/:id", (req, res) => {
	//request
		//params - url
		//body
		//http methods
		//headers

	userController.getById(req.params.id).then( result => res.send(result))
})

//in order for the routes to be use in other modules, we need to export it first
module.exports = router;